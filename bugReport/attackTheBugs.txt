/////////////////////////////////////////////////////////////////////////////////////////////
// 30th August 2016
/////////////////////////////////////////////////////////////////////////////////////////////

Decide recording area /

    Confirm button not working                                                  -- 1 (SOLVED)

    Cancel button not working                                                   -- 2 (SOLVED)

    When no area is selected but confirm button pressed, thread 
    crashes (NullPointerException)                                              -- 3 (SOLVED)
        NOW: alert window pops up instead and the exception is handled

file/ open project/

    when "Browse.." button pressed and then cancel is pressed, followed 
    by pressing open, thread crashes                                            -- 4 (SOLVED)

    when "Browse.." button pressed and then cancel is pressed, the text 
    field (that consisted of default path) 
    becomes blank (value stored null)                                           -- 5 (SOLVED)
        NOW: the text is retained when Browse..> followed by cancel is pressed

.../ffmpeg -y .../.mp4 -vf .. copy /.../.mp4 (command in Core/utils/FFMPEGWrapper 
line:89) thread crashes                                                         -- 6 (SOLVED)

../ffprobe -y ... (in the same file as above) thread crashes                    -- 7 (SOLVED)
    restructured directory to include the following folders
        resources/ffmpeg/bin/windows/...
        resources/ffmpeg/bin/macosx/...
    also changed the code to comply with the directory structure above

in the slide window when "play" button is clicked to play audio, once the audio
finishes playing, the next in line (as decided by the audio player) starts 
playing (tested on mac)                                                         -- 8 

When recording video, a box encompassing the recording area should be displayed -- 9

There is a blank sound(1 second) between the slide, need to fix it.             -- 10

////////////////////////////////////////////////////////////////////////////////////////////
// 1st September 2016
////////////////////////////////////////////////////////////////////////////////////////////

Problem number 9 solved

Once any other part of the Java app is clicked, the rectangle disappears        -- 11

maven build not working                                                         -- 12

If recording is stopped (which causes bug 11), rectangle never reappears        -- 13